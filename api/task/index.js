const { Router } = require('express');
const {
  handlerAllTask,
  handlerCreateTask,
  handlerUpdateTask,
  handlerDeleteTask,

} = require('./task.controller');
const userExtractor = require('../../middleware/userExtractor');

const router = Router();

router.get('/', handlerAllTask);
router.post('/', userExtractor, handlerCreateTask);
router.delete('/:id', handlerDeleteTask);
router.patch('/:id', handlerUpdateTask);






module.exports = router;