const jwt = require('jsonwebtoken');
const TaskModel = require('./task.model');
const UserModel = require('../user/user.model');

async function handlerAllTask(req, res) {
  const tasks = await TaskModel.find({}).populate('user',{
    username: 1,
    name: 1,
  });
  res.json(tasks);
}

 //crear tarea con relacion a usuario
async function handlerCreateTask(req, res) {
  const { 
    title,
    /* userId */
  } = req.body;

 //sacar user id de request
 const { userId } = req;
  const user = await UserModel.findById(userId);

  const NewTask = new TaskModel({
    title,
    user: user._id
  })

  try {
    const savedTask = await NewTask.save();
    /* user.tasks.push(savedTask); */
    user.tasks = user.tasks.concat(savedTask._id);
    await user.save();
    res.json(savedTask);

  } catch (error) {
    res.status(400).json({ message: 'error, task not created' });

  }
} // ACTUALIZAR TAREA

  async function handlerUpdateTask(req, res) {
    const { id } = req.params;
    const { title, isCompleted} = req.body;

    const task = await TaskModel.findByIdAndUpdate(id, { title, isCompleted }, { new: true });
    res.json(task);
    return task;
  }


  //eliminar tarea
async function handlerDeleteTask(req, res) {
  const { id } = req.params;

  const task = await TaskModel.findByIdAndDelete(id);

  try {
    res.json(task);

    
  } catch (error) {
    res.status(400).json({ message: 'error, task not deleted' });
    
  }


}




module.exports = {
  handlerAllTask,
  handlerCreateTask,
  handlerUpdateTask,
  handlerDeleteTask,
}