const { Router } = require('express');
const {handlerLogin} = require('./login.controller');

const router = Router();

router.post( '/', handlerLogin);

module.exports = router;