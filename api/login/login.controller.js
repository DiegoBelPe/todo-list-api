const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const UserModel = require('../user/user.model');

async function handlerLogin(req, res){
  const { body } = req;
  const { username, password } = body;

  const user = await UserModel.findOne({username});

  const isValid = user === null 
    ? false
    : await bcrypt.compare(password, user.passwordHash);

  if(!(user && isValid)){
    res.status(401).json({message: 'invalid credentials'});
  }

  const UserToken = {
    id: user._id,
    username: user.username,
  }

  const token = jwt.sign(UserToken, 
    process.env.JWT_SECRET,
    {
    expiresIn: 60 * 60 * 24 * 7
    }
  )

  res.send({
    name: user.name,
    username: user.username,
    token

  })

}

module.exports = {
  handlerLogin
}