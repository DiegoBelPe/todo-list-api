const { Router } = require('express');

const { handlerCreateUser, handlerAllUsers} = require('./user.controller');

const router = Router();

/* 
router.get('/:id', handlerGetUser); */
router.post('/', handlerCreateUser);
router.get('/', handlerAllUsers);

module.exports = router;