const { Schema, model } = require("mongoose");

const UserSchema = new Schema({
  username: String,
  name: String,
  passwordHash: String,
  tasks: [{ 
    type: Schema.Types.ObjectId, 
    ref: 'Task' }],
});

UserSchema.set("toJSON", {
  transform: (doc, ret) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;

    delete ret.passwordHash;
  }
});

const User = model("User", UserSchema);

module.exports = User;