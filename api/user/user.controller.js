const bcrypt = require('bcrypt'); 
const UserModel = require('./user.model');


async function handlerAllUsers(req, res) {
  const users = await UserModel.find({}).populate('tasks', {
    title: 1,
  });
  res.json(users);
}

async function handlerCreateUser(req, res) {
  const { body } = req;
  const { username, name, password } = body;

  const saltRounds = 10;
  const passwordHash = await bcrypt.hash(password, saltRounds);

  const user = new UserModel({
    username,
    name,
    passwordHash
  });

  const savedUser = await user.save();
  res.json(savedUser);


}



module.exports = {  handlerCreateUser, handlerAllUsers };