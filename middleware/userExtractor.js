const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  const authorization = req.get('Authorization');
  let token = "";
  if( authorization && authorization.toLowerCase().startsWith('bearer')){
    token = authorization.substring(7);

  }
  let decoded = {};
  try {
    decoded = jwt.verify(token, process.env.JWT_SECRET);
    
  } catch (error) {
    console.log(error);
    
  }
  if(!token || !decoded.id){
    return res.status(401).json({
      message: 'Unauthorized'
    });
  }
  const { id: userId } = decoded;
  req.userId = userId;

  next()
}