
const user = require('./api/user');
const task = require('./api/task');
const login = require('./api/login');

function routes(app){
    app.use('/api/users', user);
    app.use('/api/tasks', task);
    app.use('/api/login', login);
    
}
module.exports = routes;